import React from 'react';
import ReactDOM from 'react-dom';
import 'dotenv/config';
import { BrowserRouter as Router } from 'react-router-dom';

import { store } from './store/store';
import * as serviceWorker from './serviceWorker';
import AppRouting from './components/AppRouting';
import 'semantic-ui-css/semantic.min.css';
import 'bootstrap/dist/css/bootstrap.css';
import { Provider } from 'react-redux';

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <Router>
        <AppRouting />
      </Router>
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);

serviceWorker.unregister();
