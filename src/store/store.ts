import { createStore, compose, applyMiddleware } from 'redux';
import ThunkAction from 'redux-thunk'
import rootReducer from './rootReducer';

declare global {
    interface Window {
        __REDUX_DEVTOOLS_EXTENSION__?: Function;
        __REDUX_DEVTOOLS_EXTENSION_COMPOSE__?: typeof compose;
    }
}

const reduxDevtools = window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__();

let middlewares;
if (process.env.NODE_ENV === 'development') {
    middlewares = reduxDevtools(applyMiddleware(ThunkAction))
} else if (process.env.NODE_ENV === 'production') {
    middlewares = applyMiddleware(ThunkAction)
}

export const store = createStore(
    rootReducer,
    reduxDevtools
);