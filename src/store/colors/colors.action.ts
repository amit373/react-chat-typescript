import { Action } from 'redux';

export enum ActionTypes {
    SET_COLORS = '[Colors] Set Colors',
}

export interface SetColorsAction extends Action {
    type: ActionTypes.SET_COLORS;
    payload: any;
}

export type Actions = SetColorsAction
