import { ActionTypes } from './colors.action';

export const setColors = (colors: any) => {
    const { primary, secondary } = colors
    return {
        type: ActionTypes.SET_COLORS,
        payload: {
            primaryColor: primary,
            secondaryColor: secondary
        }
    }
}
