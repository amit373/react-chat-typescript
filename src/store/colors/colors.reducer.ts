import { Reducer } from 'redux';
import { Actions, ActionTypes } from './colors.action';

export interface ColorsState {
    primaryColor: string;
    secondaryColor: string;
}

export enum COLORS {
    PRIMARY = '#4c3c4c',
    SECONDARY = '#eee'
}

export const initalState: ColorsState = {
    primaryColor: COLORS.PRIMARY,
    secondaryColor: COLORS.SECONDARY
};

export const colorsReducer: Reducer<ColorsState, Actions> = (
    state = initalState,
    action,
) => {
    switch (action.type) {
        case ActionTypes.SET_COLORS:
            return {
                ...state,
                primaryColor: action.payload.primaryColor,
                secondaryColor: action.payload.secondaryColor
            };
        default:
            return state;
    }
};