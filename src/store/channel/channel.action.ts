import { Action } from 'redux';

export enum ActionTypes {
    SET_CURRENT_CHANNEL = '[Channel] Set Current Channel',
    SET_PRIVATE_CHANNEL = '[Channel] Set Private Channel',
    SET_USER_POSTS = '[Channel] Set User Posts'
}

export interface SetCurrentChannelAction extends Action {
    type: ActionTypes.SET_CURRENT_CHANNEL;
    payload: any;
}

export interface setPrivateChannelAction extends Action {
    type: ActionTypes.SET_PRIVATE_CHANNEL,
    payload: any
}

export interface setUserPostsAction extends Action {
    type: ActionTypes.SET_USER_POSTS,
    payload: any
}


export type Actions = SetCurrentChannelAction | setPrivateChannelAction | setUserPostsAction;
