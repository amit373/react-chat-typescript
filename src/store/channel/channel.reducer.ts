import { Reducer } from 'redux';
import { Actions, ActionTypes } from './channel.action';
import { ChannelVM } from './../../components/sidePanel/channels';

export interface ChannelState {
    currentChannel: ChannelVM | any;
    isPrivateChannel: boolean;
    userPosts: any
}

export const initalState: ChannelState = {
    currentChannel: null,
    isPrivateChannel: false,
    userPosts: null
};

export const channelReducer: Reducer<ChannelState, Actions> = (
    state = initalState,
    action,
) => {
    switch (action.type) {
        case ActionTypes.SET_CURRENT_CHANNEL:
            return {
                ...state,
                currentChannel: action.payload.currentChannel,
            };
        case ActionTypes.SET_PRIVATE_CHANNEL:
            return {
                ...state,
                isPrivateChannel: action.payload.isPrivateChannel,
            };
        case ActionTypes.SET_USER_POSTS:
            return {
                ...state,
                userPosts: action.payload.userPosts,
            };
        default:
            return state;
    }
};