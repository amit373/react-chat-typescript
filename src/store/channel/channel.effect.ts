import { ChannelVM } from './../../components/sidePanel/channels';
import { ActionTypes } from '../channel/channel.action';

export const setCurrentChannel = (channel: ChannelVM | any) => {
    return {
        type: ActionTypes.SET_CURRENT_CHANNEL,
        payload: {
            currentChannel: channel
        }
    }
}

export const setPrivateChannel = (isPrivateChannel: boolean) => {
    return {
        type: ActionTypes.SET_PRIVATE_CHANNEL,
        payload: {
            isPrivateChannel
        }
    }
}

export const setUserPosts = (userPosts: any) => {
    return {
        type: ActionTypes.SET_USER_POSTS,
        payload: {
            userPosts
        }
    }
}