import { Action } from 'redux';

export enum ActionTypes {
    SET_USER = '[Auth] Set User',
    LOGOUT_USER = '[Auth] Logout User'
}

export interface SetUserAction extends Action {
    type: ActionTypes.SET_USER;
    payload: any;
}

export interface LogoutAction extends Action {
    type: ActionTypes.LOGOUT_USER;
}

export type Actions = SetUserAction | LogoutAction

