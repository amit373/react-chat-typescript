import { Reducer } from 'redux';
import { Actions, ActionTypes } from './auth.action';

export interface AuthState {
    currentUser: any;
    isLoading: boolean
}

export const initalState: AuthState = {
    currentUser: null,
    isLoading: true
};

export const authReducer: Reducer<AuthState, Actions> = (
    state = initalState,
    action,
) => {
    switch (action.type) {
        case ActionTypes.SET_USER:
            return {
                ...state,
                currentUser: action.payload.currentUser,
                isLoading: false
            };
        case ActionTypes.LOGOUT_USER:
            return {
                ...state,
                isLoading: false
            };
        default:
            return state;
    }
};