import { ActionTypes } from './auth.action';

export const setUser = (user: any) => {
    return {
        type: ActionTypes.SET_USER,
        payload: {
            currentUser: user
        }
    }
}

export const logoutUser = () => {
    return {
        type: ActionTypes.LOGOUT_USER,
    }
}