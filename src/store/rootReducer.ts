import { combineReducers } from 'redux';
import { authReducer, AuthState } from './auth/auth.reducer';
import { channelReducer, ChannelState } from './channel/channel.reducer';
import { colorsReducer, ColorsState } from './colors/colors.reducer';

export interface AppState {
    authReducer: AuthState,
    channelReducer: ChannelState,
    colorsReducer: ColorsState
}

export default combineReducers({
    authReducer,
    channelReducer,
    colorsReducer
})
