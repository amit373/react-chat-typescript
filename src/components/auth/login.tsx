import React, { PureComponent } from 'react';
import {
  Grid,
  Form,
  Segment,
  Button,
  Header,
  Message,
  Icon,
} from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import { Formik, Field, Form as FromikForm, ErrorMessage } from 'formik';
import * as Yup from 'yup';

import { auth } from './../../shared/firebase';

interface MyFormValues {
  email: string;
  password: string;
}

class Login extends PureComponent {
  state = {
    errors: null,
  };

  render() {
    const initialValues: MyFormValues = {
      email: '',
      password: '',
    };
    return (
      <Grid textAlign='center' verticalAlign='middle' className='app'>
        <Grid.Column style={{ maxWidth: 450 }}>
          <Header as='h2' icon color='violet' textAlign='center'>
            <Icon name='code branch' color='violet' />
            Login to ReactDevChat
          </Header>
          {this.state.errors ? (
            <div className='alert alert-danger' role='alert'>
              {this.state.errors}
            </div>
          ) : null}
          <Formik
            initialValues={initialValues}
            validationSchema={Yup.object().shape({
              email: Yup.string()
                .email('Email is must be valid email')
                .required('Email is required'),
              password: Yup.string()
                .min(6, 'Password must be at least 6 characters')
                .required('Password is required'),
            })}
            onSubmit={async (values, actions) => {
              const { email, password } = values;
              const { setSubmitting } = actions;
              try {
                await auth.signInWithEmailAndPassword(email, password);
              } catch (err) {
                this.setState({ errors: err.message });
                setSubmitting(false);
              }
            }}
          >
            {({ errors, touched, isSubmitting }) => (
              <FromikForm>
                <Form size='large'>
                  <Segment stacked>
                    <Field
                      as={Form.Input}
                      fluid
                      name='email'
                      icon='mail'
                      iconPosition='left'
                      placeholder='Email'
                      type='text'
                      className={
                        errors.email && touched.email ? ' is-invalid' : ''
                      }
                      color='violet'
                    />
                    <ErrorMessage
                      name='email'
                      component='div'
                      className='invalid-feedback'
                    />
                    <Field
                      as={Form.Input}
                      fluid
                      name='password'
                      icon='lock'
                      iconPosition='left'
                      placeholder='Password'
                      type='password'
                      className={
                        errors.password && touched.password ? ' is-invalid' : ''
                      }
                      color='violet'
                    />
                    <ErrorMessage
                      name='password'
                      component='div'
                      className='invalid-feedback'
                    />
                    <Button
                      type='submit'
                      color='violet'
                      className={isSubmitting ? 'loading' : ''}
                      fluid
                      size='large'
                      disabled={isSubmitting}
                    >
                      Submit
                    </Button>
                  </Segment>
                </Form>
              </FromikForm>
            )}
          </Formik>
          <Message>
            Dont't have a account ? <Link to='/register'>Register</Link>
          </Message>
        </Grid.Column>
      </Grid>
    );
  }
}
export default Login;
