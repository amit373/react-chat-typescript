import React, { PureComponent } from 'react';
import {
  Grid,
  Form,
  Segment,
  Button,
  Header,
  Message,
  Icon,
} from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import { Formik, Field, Form as FromikForm, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import md5 from 'md5';

import { auth, db } from './../../shared/firebase';

interface MyFormValues {
  userName: string;
  email: string;
  password: string;
  confirmPassword: string;
}

class Register extends PureComponent {
  state = {
    errors: null,
    usersRef: db.ref('users'),
  };

  public saveUser = (createUser: any): Promise<any> => {
    const { usersRef } = this.state;
    return usersRef.child(createUser.user.uid).set({
      name: createUser.user.displayName,
      photoURL: createUser.user.photoURL,
    });
  };

  render() {
    const initialValues: MyFormValues = {
      userName: '',
      email: '',
      password: '',
      confirmPassword: '',
    };
    return (
      <Grid textAlign='center' verticalAlign='middle' className='app'>
        <Grid.Column style={{ maxWidth: 450 }}>
          <Header as='h2' icon color='orange' textAlign='center'>
            <Icon name='puzzle piece' color='orange' />
            Register to ReactDevChat
          </Header>
          {this.state.errors ? (
            <div className='alert alert-danger' role='alert'>
              {this.state.errors}
            </div>
          ) : null}
          <Formik
            initialValues={initialValues}
            validationSchema={Yup.object().shape({
              userName: Yup.string().required('Username is required'),
              email: Yup.string()
                .email('Email is must be valid email')
                .required('Email is required'),
              password: Yup.string()
                .min(6, 'Password must be at least 6 characters')
                .required('Password is required'),
              confirmPassword: Yup.string()
                .oneOf([Yup.ref('password'), null], 'Passwords must match')
                .required('Confirm Password is required'),
            })}
            onSubmit={async (values, actions) => {
              const { email, password, userName } = values;
              const { resetForm, setSubmitting } = actions;
              try {
                const createdUser: any = await auth.createUserWithEmailAndPassword(
                  email,
                  password
                );
                if (createdUser) {
                  try {
                    await createdUser.user
                      .updateProfile({
                        displayName: userName,
                        photoURL: `http://gravatar.com/avatar/${md5(
                          createdUser.user.email
                        )}?d=identicon`,
                      })
                      .then(() => {
                        this.saveUser(createdUser);
                      });
                    resetForm();
                    setSubmitting(false);
                  } catch (err) {
                    setSubmitting(false);
                    this.setState({ errors: err.message });
                  }
                }
              } catch (err) {
                this.setState({ errors: err.message });
                setSubmitting(false);
              }
            }}
          >
            {({ errors, status, touched, isSubmitting }) => (
              <FromikForm>
                <Form size='large'>
                  <Segment stacked>
                    <Field
                      as={Form.Input}
                      fluid
                      name='userName'
                      icon='user'
                      iconPosition='left'
                      placeholder='Username'
                      type='text'
                      className={
                        errors.userName && touched.userName ? ' is-invalid' : ''
                      }
                    />
                    <ErrorMessage
                      name='userName'
                      component='div'
                      className='invalid-feedback'
                    />
                    <Field
                      as={Form.Input}
                      fluid
                      name='email'
                      icon='mail'
                      iconPosition='left'
                      placeholder='Email'
                      type='text'
                      className={
                        errors.email && touched.email ? ' is-invalid' : ''
                      }
                    />
                    <ErrorMessage
                      name='email'
                      component='div'
                      className='invalid-feedback'
                    />
                    <Field
                      as={Form.Input}
                      fluid
                      name='password'
                      icon='lock'
                      iconPosition='left'
                      placeholder='Password'
                      type='password'
                      className={
                        errors.password && touched.password ? ' is-invalid' : ''
                      }
                    />
                    <ErrorMessage
                      name='password'
                      component='div'
                      className='invalid-feedback'
                    />
                    <Field
                      as={Form.Input}
                      fluid
                      name='confirmPassword'
                      icon='repeat'
                      iconPosition='left'
                      placeholder='Password Confirmation'
                      type='password'
                      className={
                        errors.confirmPassword && touched.confirmPassword
                          ? ' is-invalid'
                          : ''
                      }
                    />
                    <ErrorMessage
                      name='confirmPassword'
                      component='div'
                      className='invalid-feedback'
                    />
                    <Button
                      type='submit'
                      color='orange'
                      className={isSubmitting ? 'loading' : ''}
                      fluid
                      size='large'
                      disabled={isSubmitting}
                    >
                      Submit
                    </Button>
                  </Segment>
                </Form>
              </FromikForm>
            )}
          </Formik>
          <Message>
            Already a user ? <Link to='/login'>Login</Link>
          </Message>
        </Grid.Column>
      </Grid>
    );
  }
}
export default Register;
