import React from 'react';
import { Grid } from 'semantic-ui-react';
import { connect } from 'react-redux';

import ColorPanel from './colorPanel/colorPanel';
import SidePanel from './sidePanel/sidePanel';
import Messages from './messages/messages';
import MetaPanel from './metaPanel/metaPanel';

import { AppState } from '../store/rootReducer';
import { ChannelVM } from './sidePanel/channels';

import './App.scss';

interface IProps {
  currentUser: any;
  currentChannel: ChannelVM;
  isPrivateChannel: ChannelVM;
  userPosts: any;
  primaryColor: string;
  secondaryColor: string;
}

const App: React.FC<IProps> = ({
  currentUser,
  currentChannel,
  isPrivateChannel,
  userPosts,
  primaryColor,
  secondaryColor,
}) => {
  return (
    <Grid
      columns='equal'
      className='app'
      style={{ background: secondaryColor }}
    >
      <ColorPanel
        key={currentUser && currentUser.name}
        currentUser={currentUser}
      />
      <SidePanel
        key={currentUser && currentUser.uid}
        currentUser={currentUser}
        primaryColor={primaryColor}
      />

      <Grid.Column style={{ marginLeft: 320 }}>
        <Messages
          key={currentChannel && currentChannel.id}
          currentChannel={currentChannel}
          currentUser={currentUser}
          isPrivateChannel={isPrivateChannel}
        />
      </Grid.Column>

      <Grid.Column width={4}>
        <MetaPanel
          key={currentChannel && currentChannel.name}
          userPosts={userPosts}
          currentChannel={currentChannel}
          isPrivateChannel={isPrivateChannel}
        />
      </Grid.Column>
    </Grid>
  );
};

const mapStateToProps = (state: AppState) => {
  const { authReducer, channelReducer, colorsReducer } = state;
  const { currentUser } = authReducer;
  const { currentChannel, isPrivateChannel, userPosts } = channelReducer;
  const { primaryColor, secondaryColor } = colorsReducer;
  return {
    currentUser,
    currentChannel,
    isPrivateChannel,
    userPosts,
    primaryColor,
    secondaryColor,
  };
};
export default connect(mapStateToProps, null)(App);
