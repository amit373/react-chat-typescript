import React, { PureComponent } from 'react';
import {
  Segment,
  Header,
  Accordion,
  Icon,
  Image,
  List,
} from 'semantic-ui-react';

import { ChannelVM } from '../sidePanel/channels';
import { format } from './../../services/time.service';
interface IProps {
  isPrivateChannel: ChannelVM;
  currentChannel: ChannelVM;
  userPosts: any;
}

class MetaPanel extends PureComponent<IProps> {
  state = {
    activeIndex: 0,
    userPosts: this.props.userPosts,
  };

  public setActiveIndex = (event: any, titleProps: any): void => {
    const { index } = titleProps;
    const { activeIndex } = this.state;
    const newIndex = activeIndex === index ? -1 : index;
    this.setState({ activeIndex: newIndex });
  };

  private formateCount = (count: number): any => {
    return count > 1 || count === 0 ? `${count} posts` : `${count} post`;
  };

  public displayTopPosters = (posts: any): JSX.Element[] => {
    const obj = Object.entries(posts)
      .sort((a: any, b: any) => b[1] - a[1])
      .map(([key, val]: any, i) => (
        <List.Item key={i}>
          <Image avatar src={val.photoURL} />
          <List.Content>
            <List.Header as={'a'}>{key}</List.Header>
            <List.Description>{this.formateCount(val.count)}</List.Description>
          </List.Content>
        </List.Item>
      ))
      .slice(0, 5);
    return obj;
  };

  render() {
    const { activeIndex } = this.state;
    const { isPrivateChannel, currentChannel, userPosts } = this.props;
    if (isPrivateChannel) return null;
    return (
      <>
        <Segment loading={!currentChannel}>
          <Header as='h3' attached='top'>
            About # {currentChannel?.name}
          </Header>
          <Accordion styled attached='true'>
            <Accordion.Title
              active={activeIndex === 0}
              index={0}
              onClick={this.setActiveIndex}
            >
              <Icon name='dropdown' />
              <Icon name='info' />
              Channel Details
            </Accordion.Title>
            <Accordion.Content active={activeIndex === 0}>
              {currentChannel?.details}
            </Accordion.Content>
            <Accordion.Title
              active={activeIndex === 1}
              index={1}
              onClick={this.setActiveIndex}
            >
              <Icon name='dropdown' />
              <Icon name='user circle' />
              Top Posters
            </Accordion.Title>
            <Accordion.Content active={activeIndex === 1}>
              <List>{userPosts && this.displayTopPosters(userPosts)}</List>
            </Accordion.Content>
            <Accordion.Title
              active={activeIndex === 2}
              index={2}
              onClick={this.setActiveIndex}
            >
              <Icon name='dropdown' />
              <Icon name='pencil alternate' />
              Created By
            </Accordion.Title>
            <Accordion.Content active={activeIndex === 2}>
              <Header as='h3'>
                <Image circular src={currentChannel?.createdBy?.photoURL} />{' '}
                {currentChannel?.createdBy?.name}
              </Header>
            </Accordion.Content>
            <Header color='grey' attached='top'>
              Created: {format(currentChannel?.createdAt, 'LL')}
            </Header>
          </Accordion>
        </Segment>
      </>
    );
  }
}
export default MetaPanel;
