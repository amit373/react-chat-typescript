import React from 'react';
import { Menu } from 'semantic-ui-react';

import UserPanel from './userPanel';
import Channels from './channels';
import DirectMessages from './directMessages';
import Starred from './starred';

interface IProps {
  currentUser: any;
  primaryColor: string;
}

class SidePanel extends React.PureComponent<IProps> {
  render() {
    const { currentUser, primaryColor } = this.props;
    const menuStyles = {
      background: primaryColor,
      fontSize: '1.2rem',
    };
    return (
      <Menu size='large' inverted fixed='left' vertical style={menuStyles}>
        <UserPanel primaryColor={primaryColor} currentUser={currentUser} />
        <Starred currentUser={currentUser} />
        <Channels currentUser={currentUser} />
        <DirectMessages currentUser={currentUser} />
      </Menu>
    );
  }
}
export default SidePanel;
