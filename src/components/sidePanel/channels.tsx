import React from 'react';
import { Dispatch, bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {
  Formik,
  Field as FormikField,
  Form as FromikForm,
  ErrorMessage,
} from 'formik';
import * as Yup from 'yup';
import {
  Menu,
  Icon,
  Modal,
  Form,
  Button,
  Input,
  Label,
} from 'semantic-ui-react';

import { db } from '../../shared/firebase';
import * as firebase from 'firebase';
import { AppState } from '../../store/rootReducer';
import * as channelActions from './../../store/channel/channel.effect';

interface IProps {
  actions?: any;
  currentUser: any;
  currentChannel: ChannelVM;
}

interface MyFormValues {
  channelName: string;
  channelDetails: string;
}

export interface ChannelVM {
  id: string | number | any;
  name: string;
  details: string;
  createdAt: Date | Object;
  createdBy: CreatedByVM;
}

export interface CreatedByVM {
  name: string;
  photoURL: string;
}

class Channels extends React.PureComponent<IProps> {
  state = {
    activeChannel: '',
    channel: null,
    channels: [],
    channelsRef: db.ref('channels'),
    firstLoad: true,
    messageRef: db.ref('messages'),
    notifications: [],
    modalOpen: false,
    user: this.props.currentUser,
    typingRef: db.ref('typing'),
  };

  public componentDidMount(): void {
    this.addListeners();
  }

  public componentWillUnmount(): void {
    this.removeListeners();
  }

  public handleOpen = (): void => this.setState({ modalOpen: true });

  public handleClose = (resetForm: any): void => {
    if (typeof resetForm === 'function') resetForm();
    this.setState({ modalOpen: false });
  };

  protected addListeners = (): void => {
    const { channelsRef } = this.state;
    this.setState({ channels: [] });
    channelsRef.on('child_added', (snapshot) => {
      let loadedChannels: Array<any>[] = [...this.state.channels];
      loadedChannels.push(snapshot.val());
      this.setState({ channels: loadedChannels }, () => this.setFirstChannel());
      this.addNotificationListener(snapshot.key);
    });
  };

  private addNotificationListener = (channelId: string | any): void => {
    this.state.messageRef.child(channelId).on('value', (snapshot) => {
      if (this.state.channel) {
        this.handleNotifications(
          channelId,
          (this.state.channel as any).id,
          this.state.notifications,
          snapshot
        );
      }
    });
  };

  private handleNotifications = (
    channelId: string,
    currentChannelId: string,
    notifications: Array<any>,
    snapshot: any
  ): void => {
    let lastTotal: number = 0;
    let index = notifications.findIndex(
      (notification) => notification.id === channelId
    );
    if (index !== -1) {
      if (channelId !== currentChannelId) {
        lastTotal = notifications[index].total;

        if (snapshot.numChildren() - lastTotal > 0) {
          notifications[index].count = snapshot.numChildren();
        }
      }
      notifications[index].lastKnownTotal = snapshot.numChildren();
    } else {
      notifications.push({
        id: channelId,
        total: snapshot.numChildren(),
        lastKnownTotal: snapshot.numChildren(),
        count: 0,
      });
    }
    this.setState({ notifications });
  };

  public removeListeners = (): void => {
    this.state.channelsRef.off();
  };

  public changeChannel = (channel: ChannelVM): void => {
    const { currentUser } = this.props;
    const { typingRef } = this.state;
    this.setActiveChannel(channel);
    typingRef
      .child((this.state.channel as any).id)
      .child(currentUser.uid)
      .remove();
    this.clearNotifications();
    this.props.actions.setCurrentChannel(channel);
    this.props.actions.setPrivateChannel(false);
    this.setState({ channel });
  };

  private clearNotifications = (): void => {
    let index = this.state.notifications.findIndex(
      (notification: any) => notification.id === (this.state.channel as any).id
    );
    if (index !== -1) {
      let updatedNotifications: Array<any> = [...this.state.notifications];
      updatedNotifications[index].total = (this.state.notifications[
        index
      ] as any).lastKnownTotal;
      updatedNotifications[index].count = 0;
      this.setState({ notifications: updatedNotifications });
    }
  };

  private setFirstChannel = (): void => {
    const { channels, firstLoad } = this.state;
    const firstChannel: ChannelVM = channels[0];
    if (firstLoad && channels.length > 0) {
      this.props.actions.setCurrentChannel(firstChannel);
      this.setActiveChannel(firstChannel);
      this.setState({ channel: firstChannel });
    }
    this.setState({ firstLoad: false });
  };

  private setActiveChannel = (channel: ChannelVM): void => {
    this.setState({ activeChannel: channel.id });
  };

  private getNotificationCount = (channel: ChannelVM): number | undefined => {
    let count: number = 0;
    this.state.notifications.forEach((notification: any) => {
      if (notification.id === channel.id) {
        count = notification.count;
      }
    });
    if (count > 0) return count;
  };

  private displayChannels = (channels: Array<any>): any => {
    const { activeChannel } = this.state;
    const obj =
      channels.length > 0 &&
      channels.map((channel: ChannelVM) => (
        <Menu.Item
          key={channel.id}
          onClick={() => this.changeChannel(channel)}
          name={channel.name}
          style={{ opacity: 0.7 }}
          active={channel.id === activeChannel}
        >
          {this.getNotificationCount(channel) && (
            <Label color='red'>{this.getNotificationCount(channel)}</Label>
          )}
          #{channel.name}
        </Menu.Item>
      ));
    return obj;
  };

  render() {
    const { channels, modalOpen } = this.state;
    const initialValues: MyFormValues = {
      channelName: '',
      channelDetails: '',
    };
    return (
      <React.Fragment>
        <Menu.Menu className='menu'>
          <Menu.Item>
            <span>
              <Icon name='exchange' /> CHANNELS
            </span>{' '}
            ({channels.length}) <Icon name='add' onClick={this.handleOpen} />
          </Menu.Item>
          {this.displayChannels(channels)}
        </Menu.Menu>
        {/* Add Channel Modal */}
        <Formik
          initialValues={initialValues}
          validationSchema={Yup.object().shape({
            channelName: Yup.string().required('Channel Name is required'),
            channelDetails: Yup.string().required(
              'Channel Details is required'
            ),
          })}
          onSubmit={(values, actions) => {
            const { setSubmitting, resetForm } = actions;
            const { channelName, channelDetails } = values;
            const { channelsRef, user } = this.state;
            const key: string | any = channelsRef.push().key;

            const newChannel: ChannelVM = {
              id: key,
              name: channelName,
              details: channelDetails,
              createdBy: {
                name: user.displayName,
                photoURL: user.photoURL,
              },
              createdAt: firebase.database.ServerValue.TIMESTAMP,
            };

            channelsRef
              .child(key)
              .update(newChannel)
              .then(() => {
                setSubmitting(false);
                this.handleClose(resetForm);
              })
              .catch((err) => {
                console.error(err);
                setSubmitting(false);
                resetForm();
              });
          }}
        >
          {({ errors, touched, isSubmitting, resetForm }) => (
            <FromikForm>
              <Modal basic open={modalOpen} onClose={this.handleClose}>
                <Modal.Header>Add a Channel</Modal.Header>
                <Form>
                  <Modal.Content>
                    <FormikField as={Form.Field}>
                      <Input
                        as={Input}
                        fluid
                        label='Name of Channel'
                        name='channelName'
                        className={
                          errors.channelName && touched.channelName
                            ? ' is-invalid'
                            : ''
                        }
                      />
                      <ErrorMessage
                        name='channelName'
                        component='div'
                        className='invalid-feedback'
                      />
                    </FormikField>

                    <FormikField as={Form.Field}>
                      <Input
                        as={Input}
                        fluid
                        label='About the Channel'
                        name='channelDetails'
                        className={
                          errors.channelDetails && touched.channelDetails
                            ? ' is-invalid'
                            : ''
                        }
                      />
                      <ErrorMessage
                        name='channelDetails'
                        component='div'
                        className='invalid-feedback'
                      />
                    </FormikField>
                  </Modal.Content>
                  <Modal.Actions style={{ float: 'right', marginTop: '13px' }}>
                    <Button
                      type='submit'
                      className={isSubmitting ? 'loading' : ''}
                      disabled={isSubmitting}
                      color='green'
                      inverted
                    >
                      <Icon name='checkmark' /> Add
                    </Button>
                    <Button
                      color='red'
                      inverted
                      onClick={this.handleClose.bind(null, resetForm)}
                    >
                      <Icon name='remove' /> Cancel
                    </Button>
                  </Modal.Actions>
                </Form>
              </Modal>
            </FromikForm>
          )}
        </Formik>
      </React.Fragment>
    );
  }
}
const mapStateToProps = (state: AppState) => {
  const { channelReducer } = state;
  const { currentChannel } = channelReducer;
  return {
    currentChannel,
  };
};

const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    actions: {
      ...bindActionCreators(channelActions, dispatch),
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(Channels);
