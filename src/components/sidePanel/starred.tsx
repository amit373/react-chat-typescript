import React, { PureComponent } from 'react';
import { Menu, Icon } from 'semantic-ui-react';

import { ChannelVM } from './channels';
import { bindActionCreators, Dispatch } from 'redux';
import { connect } from 'react-redux';
import * as channelActions from './../../store/channel/channel.effect';
import { db } from '../../shared/firebase';

interface IProps {
  actions?: any;
  currentUser: any;
}

class Starred extends PureComponent<IProps> {
  state = {
    usersRef: db.ref('users'),
    activeChannel: '',
    starredChannels: [],
  };

  componentDidMount() {
    if (this.props.currentUser) {
      this.addListeners(this.props.currentUser.uid);
    }
  }

  componentWillUnmount() {
    this.removeListener();
  }

  removeListener = () => {
    if (this.props.currentUser) {
      this.state.usersRef.child(`${this.props.currentUser.uid}/starred`).off();
    }
  };

  addListeners = (userId: string) => {
    this.state.usersRef
      .child(userId)
      .child('starred')
      .on('child_added', (snap) => {
        const starredChannel = { id: snap.key, ...snap.val() };
        this.setState({
          starredChannels: [...this.state.starredChannels, starredChannel],
        });
      });

    this.state.usersRef
      .child(userId)
      .child('starred')
      .on('child_removed', (snap) => {
        const channelToRemove = { id: snap.key, ...snap.val() };
        const filteredChannels = this.state.starredChannels.filter(
          (channel: ChannelVM) => {
            return channel.id !== channelToRemove.id;
          }
        );
        this.setState({ starredChannels: filteredChannels });
      });
  };

  setActiveChannel = (channel: ChannelVM) => {
    this.setState({ activeChannel: channel.id });
  };

  changeChannel = (channel: ChannelVM) => {
    this.setActiveChannel(channel);
    this.props.actions.setCurrentChannel(channel);
    this.props.actions.setPrivateChannel(false);
  };

  displayChannels = (starredChannels: Array<any>) =>
    starredChannels.length > 0 &&
    starredChannels.map((channel: ChannelVM) => (
      <Menu.Item
        key={channel.id}
        onClick={() => this.changeChannel(channel)}
        name={channel.name}
        style={{ opacity: 0.7 }}
        active={channel.id === this.state.activeChannel}
      >
        # {channel.name}
      </Menu.Item>
    ));

  render() {
    const { starredChannels } = this.state;
    return (
      <>
        <Menu.Menu className='menu'>
          <Menu.Item>
            <span>
              <Icon name='star' /> STARRED
            </span>{' '}
            ({starredChannels.length})
          </Menu.Item>
          {this.displayChannels(starredChannels)}
        </Menu.Menu>
      </>
    );
  }
}

const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    actions: {
      ...bindActionCreators(channelActions, dispatch),
    },
  };
};
export default connect(null, mapDispatchToProps)(Starred);
