import React from 'react';
import {
  Grid,
  Header,
  Icon,
  Dropdown,
  Image,
  Modal,
  Input,
  Button,
} from 'semantic-ui-react';
import AvatarEditor from 'react-avatar-editor';

import { auth, db, storage } from './../../shared/firebase';

interface IProps {
  currentUser: any;
  primaryColor: string;
}

class UserPanel extends React.PureComponent<IProps> {
  state = {
    modal: false,
    previewImage: '',
    croppedImage: '',
    blob: '',
    uploadedCroppedImage: '',
    storageRef: storage.ref(),
    currentUserRef: auth.currentUser,
    usersRef: db.ref('users'),
    metadata: {
      contentType: 'image/jpeg',
    },
  };
  avatarEditor: any = React.createRef();
  dropdownOptions = (): any => {
    return [
      {
        key: 'user',
        text: (
          <span>
            Signed in as{' '}
            <strong>
              {this.props.currentUser?.displayName !== null
                ? this.props.currentUser?.displayName
                : 'N/A'}
            </strong>
          </span>
        ),
        disabled: true,
      },
      {
        key: 'avatar',
        text: <span onClick={this.openModal}>Change Avatar</span>,
      },
      {
        key: 'signout',
        text: <span onClick={this.handleSignout}>Sign Out</span>,
      },
    ];
  };

  public handleSignout = (): void => {
    auth.signOut();
  };

  public handleImageChange = (
    event: React.ChangeEvent<HTMLInputElement>
  ): void => {
    const file = event.target.files![0];
    const reader = new FileReader();

    if (file) {
      reader.readAsDataURL(file);
      reader.addEventListener('load', () => {
        this.setState({ previewImage: reader.result });
      });
    }
  };

  public handleCropImage = (): void => {
    if (
      this.avatarEditor &&
      this.avatarEditor !== undefined &&
      this.avatarEditor !== null
    ) {
      this.avatarEditor.getImageScaledToCanvas().toBlob((blob: Blob) => {
        if (blob) {
          let imageURL = URL.createObjectURL(blob);
          this.setState({ croppedImage: imageURL, blob });
        }
      });
    }
  };

  public uploadCroppedImage = (): void => {
    const { currentUserRef, storageRef, blob, metadata } = this.state;
    const file: any = blob;
    storageRef
      .child(`avatar/user-${currentUserRef?.uid}`)
      .put(file, metadata)
      .then((snapshot) => {
        snapshot.ref.getDownloadURL().then((downloadURL: string) => {
          this.setState({ uploadCroppedImage: downloadURL }, () => {
            this.changeAvatar(downloadURL);
          });
        });
      });
  };

  public changeAvatar = async (downloadURL: string): Promise<void> => {
    await this.state.currentUserRef
      ?.updateProfile({
        photoURL: downloadURL,
      })
      .then(() => {
        console.log('photoURL updated');
        this.closeModal();
      })
      .catch((err) => {
        console.error(err);
      });
    await this.state.usersRef
      .child(this.props.currentUser.uid)
      .update({ photoURL: downloadURL })
      .then(() => {
        console.log('User Avatar Updated');
      })
      .catch((err) => {
        console.error(err);
      });
  };

  public openModal = (): void => this.setState({ modal: true });

  public closeModal = (): void => this.setState({ modal: false });

  render() {
    const { currentUser, primaryColor } = this.props;
    const { modal, previewImage, croppedImage } = this.state;
    return (
      <Grid style={{ background: primaryColor }}>
        <Grid.Column>
          <Grid.Row style={{ padding: '1.2em 1.2em 1.2em 0.2em', margin: 0 }}>
            <Header inverted floated='left' as='h2'>
              <Icon name='code' />
              <Header.Content>React Chat</Header.Content>
            </Header>
            <Header style={{ padding: '0.25em' }} as='h4' inverted>
              <Dropdown
                trigger={
                  <span>
                    <Image src={currentUser?.photoURL} spaced='right' avatar />
                    {currentUser?.displayName}
                  </span>
                }
                options={this.dropdownOptions()}
              />
            </Header>
          </Grid.Row>

          <Modal basic open={modal} onClose={this.closeModal}>
            <Modal.Header>Change Avatar</Modal.Header>
            <Modal.Content>
              <Input
                onChange={this.handleImageChange}
                fluid
                type='file'
                label='New Avatar'
                name='previewImage'
              />
              <Grid centered stackable columns={2}>
                <Grid.Row centered>
                  <Grid.Column className='ui center aligned grid'>
                    {previewImage && (
                      <AvatarEditor
                        ref={(node) => (this.avatarEditor = node)}
                        image={previewImage}
                        width={120}
                        height={120}
                        border={50}
                        scale={1.2}
                      />
                    )}
                  </Grid.Column>
                  <Grid.Column>
                    {croppedImage && (
                      <Image
                        style={{ margin: '3.5em auto' }}
                        width={100}
                        height={100}
                        src={croppedImage}
                      />
                    )}
                  </Grid.Column>
                </Grid.Row>
              </Grid>
            </Modal.Content>
            <Modal.Actions>
              {croppedImage && (
                <Button
                  color='green'
                  inverted
                  onClick={this.uploadCroppedImage}
                >
                  <Icon name='save' /> Change Avatar
                </Button>
              )}
              <Button color='green' inverted onClick={this.handleCropImage}>
                <Icon name='image' /> Preview
              </Button>
              <Button color='red' inverted onClick={this.closeModal}>
                <Icon name='remove' /> Cancel
              </Button>
            </Modal.Actions>
          </Modal>
        </Grid.Column>
      </Grid>
    );
  }
}

export default UserPanel;
