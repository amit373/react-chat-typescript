import React, { PureComponent } from 'react';
import { Menu, Icon } from 'semantic-ui-react';
import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';

import { db } from '../../shared/firebase';
import * as channelActions from '../../store/channel/channel.effect';

interface IProps {
  currentUser: any;
  actions?: any;
}

interface channelDataVM {
  id: string;
  name: string;
  isPrivate: boolean;
}

class DirectMessages extends PureComponent<IProps> {
  state = {
    activeChannel: '',
    users: [],
    usersRef: db.ref('users'),
    connectedRef: db.ref('.info/connected'),
    presenceRef: db.ref('presence'),
  };

  componentDidMount() {
    if (this.props.currentUser) {
      this.addListeners(this.props.currentUser.uid);
    }
  }

  componentWillUnmount() {
    this.removeListeners();
  }

  removeListeners = () => {
    this.state.usersRef.off();
    this.state.presenceRef.off();
    this.state.connectedRef.off();
  };

  addListeners = (currentUserId: string) => {
    let loadedUsers: Array<any> = [...this.state.users];
    this.state.usersRef.on('child_added', (snapshot) => {
      if (currentUserId !== snapshot.key) {
        let user: any = snapshot.val();
        user.uid = snapshot.key;
        user.status = 'offline';
        loadedUsers.push(user);
        this.setState({ users: loadedUsers });
      }
    });

    this.state.connectedRef.on('value', (snapshot) => {
      if (snapshot.val() === true) {
        const ref = this.state.presenceRef.child(currentUserId);
        ref.set(true);
        ref.onDisconnect().remove((err) => {
          if (err !== null) {
            console.error(err);
          }
        });
      }
    });

    this.state.presenceRef.on('child_added', (snapshot) => {
      if (currentUserId !== snapshot.key) {
        this.addStatusToUser(snapshot.key);
      }
    });

    this.state.presenceRef.on('child_removed', (snapshot) => {
      if (currentUserId !== snapshot.key) {
        this.addStatusToUser(snapshot.key, false);
      }
    });
  };

  addStatusToUser = (userId: string | null, connected: boolean = true) => {
    const updatedUsers = this.state.users.reduce((acc: any, user: any) => {
      if (user.uid === userId) {
        user.status = `${connected ? 'online' : 'offline'}`;
        return acc.concat(user);
      }
    }, []);
    this.setState({ users: updatedUsers });
  };

  isUserOnline = (user: any) => user.status === 'online';

  changeChannel = (user: any) => {
    const channelId = this.getChannelId(user.uid);
    const channelData: channelDataVM = {
      id: channelId,
      name: user.name,
      isPrivate: true,
    };
    this.props.actions.setCurrentChannel(channelData);
    this.props.actions.setPrivateChannel(true);
    this.setActiveChannel(user.uid);
  };

  getChannelId = (userId: string) => {
    const currentUserId = this.props.currentUser.uid;
    return userId < currentUserId
      ? `${userId}/${currentUserId}`
      : `${currentUserId}/${userId}`;
  };

  setActiveChannel = (userId: string) => {
    this.setState({ activeChannel: userId });
  };

  render() {
    const { users, activeChannel } = this.state;
    return (
      <Menu.Menu className='menu'>
        <Menu.Item>
          <span>
            <Icon name='mail' /> DIRECT MESSAGES
          </span>{' '}
          ({users.length})
        </Menu.Item>
        {users.map((user: any) => (
          <Menu.Item
            key={user.uid}
            active={user.uid === activeChannel}
            onClick={() => this.changeChannel(user)}
            style={{
              opactiy: 0.7,
              fontStyle: 'italic',
            }}
          >
            <Icon
              name='circle'
              color={this.isUserOnline(user) ? 'green' : 'red'}
            />
            @ {user.name}
          </Menu.Item>
        ))}
      </Menu.Menu>
    );
  }
}

const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    actions: {
      ...bindActionCreators(channelActions, dispatch),
    },
  };
};
export default connect(null, mapDispatchToProps)(DirectMessages);
