import React, { PureComponent } from 'react';
import {
  Switch,
  Route,
  Redirect,
  withRouter,
  RouteComponentProps,
} from 'react-router-dom';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { bindActionCreators } from 'redux';

import App from './App';
import Login from './auth/login';
import Register from './auth/register';
import Spinner from './spinner/spinner';

import { auth } from './../shared/firebase';
import * as authActions from './../store/auth/auth.effects';

class Root extends PureComponent<any> {
  componentDidMount() {
    auth.onAuthStateChanged((user: any) => {
      const { actions, history } = this.props;
      if (user) {
        actions.setUser(user);
        history.push('/');
      } else {
        history.push('/login');
        actions.logoutUser();
      }
    });
  }

  render() {
    return this.props.isLoading ? (
      <Spinner />
    ) : (
      <Switch>
        <Route exact path='/' component={App} />
        <Route exact path='/login' component={Login} />
        <Route exact path='/register' component={Register} />
        <Route exact path='**' component={App}>
          <Redirect to='' />
        </Route>
      </Switch>
    );
  }
}

const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    actions: {
      ...bindActionCreators(authActions, dispatch),
    },
  };
};
export default withRouter(connect(null, mapDispatchToProps)(Root));
