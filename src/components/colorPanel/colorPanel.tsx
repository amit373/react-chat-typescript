import React, { PureComponent, Fragment } from 'react';
import {
  Sidebar,
  Divider,
  Button,
  Menu,
  Modal,
  Icon,
  Label,
  Segment,
} from 'semantic-ui-react';
import { SliderPicker } from 'react-color';
import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';

import { db } from '../../shared/firebase';
import * as colorsActions from './../../store/colors/colors.effects';
interface IProps {
  currentUser: any;
  actions?: any;
}

class ColorPanel extends PureComponent<IProps> {
  state = {
    isModal: false,
    primary: '',
    secondary: '',
    usersRef: db.ref('users'),
    userColors: [],
  };

  public componentDidMount(): void {
    if (this.props.currentUser) {
      this.addListener(this.props.currentUser.uid);
    }
  }

  public componentWillUnmount(): void {
    this.removeListener();
  }

  private removeListener = (): void => {
    if (this.props.currentUser) {
      this.state.usersRef.child(`${this.props.currentUser.uid}/colors`).off();
    }
  };

  private addListener = (userId: string): void => {
    let userColors: Array<any> = [];
    this.state.usersRef.child(`${userId}/colors`).on('child_added', (snap) => {
      userColors.unshift(snap.val());
      this.setState({ userColors });
    });
  };

  public handleChangePrimary = (color: any): void => {
    this.setState({ primary: color.hex });
  };

  public handleChangeSecondary = (color: any): void => {
    this.setState({ secondary: color.hex });
  };

  protected handleSaveColors = (): void => {
    if (this.state.primary && this.state.secondary) {
      this.saveColors(this.state.primary, this.state.secondary);
    }
  };

  public checkColors = async (): Promise<boolean> => {
    let isLimit: boolean = false;
    const snapshot = await this.state.usersRef
      .child(`${this.props.currentUser.uid}/colors`)
      .once('value');
    if (snapshot.exists())
      isLimit = Object.values(snapshot.val()).length === 3 ? true : false;
    return isLimit;
  };

  private saveColors = async (
    primary: string,
    secondary: string
  ): Promise<void> => {
    const isLimit = await this.checkColors();
    if (isLimit) {
      alert('Only 3 colors to be added!');
    } else {
      await this.state.usersRef
        .child(`${this.props.currentUser.uid}/colors`)
        .push()
        .update({
          primary,
          secondary,
        })
        .then(() => {
          this.setState({ primary: '', secondary: '' });
          this.closeModal();
        })
        .catch((err) => {
          console.error(err);
        });
    }
  };

  private displayUserColors = (colors: Array<any>): false | JSX.Element[] => {
    return (
      colors.length > 0 &&
      colors.map((color, i) => (
        <Fragment key={i}>
          <Divider />
          <div
            className='color__container'
            onClick={() => this.props.actions.setColors(color)}
          >
            <div
              className='color__square'
              style={{ background: color.primary }}
            >
              <div
                className='color__overlay'
                style={{ background: color.secondary }}
              />
            </div>
          </div>
        </Fragment>
      ))
    );
  };

  public openModal = (): void => this.setState({ isModal: true });

  public closeModal = (): void => this.setState({ isModal: false });

  render() {
    const { isModal, primary, secondary, userColors } = this.state;
    return (
      <Sidebar
        as={Menu}
        icon='labeled'
        inverted
        vertical
        visible
        width='very thin'
      >
        <Divider />
        <Button
          icon='add'
          size='small'
          onClick={() => this.openModal()}
          color='blue'
        />
        {this.displayUserColors(userColors)}
        <Modal basic open={isModal} onClose={() => this.closeModal()}>
          <Modal.Header>Choose App Colors</Modal.Header>
          <Modal.Content>
            <Segment inverted>
              <Label content='Primary Color' />
              <SliderPicker
                color={primary}
                onChange={this.handleChangePrimary}
              />
            </Segment>
            <Segment inverted>
              <Label content='Secondary Color' />
              <SliderPicker
                color={secondary}
                onChange={this.handleChangeSecondary}
              />
            </Segment>
          </Modal.Content>
          <Modal.Actions>
            <Button color='green' inverted onClick={this.handleSaveColors}>
              <Icon name='checkmark' inverted /> Save Colors
            </Button>
            <Button color='red' inverted onClick={() => this.closeModal()}>
              <Icon name='remove' /> Cancel
            </Button>
          </Modal.Actions>
        </Modal>
      </Sidebar>
    );
  }
}

const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    actions: {
      ...bindActionCreators(colorsActions, dispatch),
    },
  };
};
export default connect(null, mapDispatchToProps)(ColorPanel);
