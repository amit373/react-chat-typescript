import React, { PureComponent } from 'react';
import { lookup } from 'mime-types';
import { Modal, Input, Button, Icon } from 'semantic-ui-react';

interface IProps {
  modal: any;
  closeModal: () => void;
  uploadFile: (file: HTMLInputElement, metaData: any) => void;
}

class FileModal extends PureComponent<IProps> {
  state = {
    file: null,
    authorized: ['image/jpeg', 'image/jpg', 'image/png'],
  };

  private addFile = (event: React.ChangeEvent<HTMLInputElement>): void => {
    const file = event.target.files![0];
    if (file) {
      this.setState({ file });
    }
  };

  private sendFile = (): void => {
    const file: any = this.state.file;
    const { uploadFile, closeModal } = this.props;
    if (file !== null) {
      if (this.isAuthorized(file.name)) {
        const metaData = { contentType: lookup(file.name) };
        uploadFile(file, metaData);
        closeModal();
        this.clearFile();
      }
    }
  };

  private isAuthorized = (fileName: string): any => {
    const isLookop: any = lookup(fileName);
    return this.state.authorized.includes(isLookop);
  };

  private clearFile = (): void => this.setState({ file: null });

  render() {
    const { modal, closeModal } = this.props;
    return (
      <Modal basic open={modal} onClose={closeModal}>
        <Modal.Header>Select an Image File</Modal.Header>
        <Modal.Content>
          <Input
            fluid
            label='File types: jpg, png'
            name='file'
            type='file'
            onChange={this.addFile}
          />
        </Modal.Content>
        <Modal.Actions>
          <Button color='green' inverted onClick={this.sendFile}>
            <Icon name='checkmark' />
            Send
          </Button>
          <Button color='red' inverted onClick={closeModal}>
            <Icon name='remove' />
            Cancel
          </Button>
        </Modal.Actions>
      </Modal>
    );
  }
}
export default FileModal;
