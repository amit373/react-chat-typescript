import React, { PureComponent } from 'react';
import { Segment, Input, Button } from 'semantic-ui-react';
import 'emoji-mart/css/emoji-mart.css';
import { Picker, emojiIndex } from 'emoji-mart';

import { db, storage } from '../../shared/firebase';
import { ChannelVM } from '../sidePanel/channels';
import * as firebase from 'firebase';

import FileModal from './FileModal';
import ProgressBar from './progressBar';

/*
  TODO:
  Add on child_remove and on child_changed callbacks
*/
export interface MessageVM {
  createdAt: Date | Object;
  user: UserVM;
  content?: string;
  image?: string | null;
}

export interface UserVM {
  id: string;
  name: string;
  photoURL: string;
}

interface IProps {
  currentChannel: ChannelVM;
  currentUser: any;
  isProgressBarVisible: (percentUploaded: number) => void;
  isPrivateChannel: ChannelVM;
  getMessageRef: any;
}

class MessageForm extends PureComponent<IProps> {
  state = {
    errors: {
      message: '',
    },
    emojiPicker: false,
    loading: false,
    message: '',
    messagesRef: db.ref('messages'),
    modal: false,
    percentUploaded: 0,
    uploadTask: null,
    uploadState: '',
    storageRef: storage.ref(),
    typingRef: db.ref('typing'),
  };

  componentWillUnmount() {
    if (this.state.uploadTask !== null) {
      (this.state.uploadTask as any).cancel();
      this.setState({ uploadTask: null });
    }
  }

  messageInputRef: any = React.createRef();

  private openModal = (): void => this.setState({ modal: true });

  private closeModal = (): void => this.setState({ modal: false });

  private handleChange = (event: React.ChangeEvent<HTMLInputElement>): void => {
    this.setState({ [event.target.name]: event.target.value });
  };

  private handleKeyDown = (event: React.KeyboardEvent) => {
    if (
      (event.ctrlKey && event.keyCode === 13) ||
      (event.shiftKey && event.keyCode === 13)
    ) {
      return;
    }
    if (event.keyCode === 13) {
      this.sendMessage();
    }
    const { message, typingRef } = this.state;
    const { currentUser, currentChannel } = this.props;
    if (message) {
      typingRef
        .child(currentChannel.id)
        .child(currentUser.uid)
        .set(currentUser.displayName);
    } else {
      typingRef.child(currentChannel.id).child(currentUser.uid).remove();
    }
  };

  private handleTogglePicker = () => {
    this.setState({ emojiPicker: !this.state.emojiPicker });
  };

  private handleAddEmoji = (emoji: any) => {
    const oldMessage = this.state.message;
    const newMessage = this.colonToUnicode(` ${oldMessage} ${emoji.colons} `);
    this.setState({ message: newMessage, emojiPicker: false });
    setTimeout(() => this.messageInputRef.focus(), 0);
  };

  private colonToUnicode = (message: string) => {
    return message.replace(/:[A-Za-z0-9_+-]+:/g, (x: any) => {
      x = x.replace(/:/g, '');
      let emoji: any = emojiIndex.emojis[x];
      if (typeof emoji !== 'undefined') {
        let unicode = emoji.native;
        if (typeof unicode !== 'undefined') {
          return unicode;
        }
      }
      x = ':' + x + ':';
      return x;
    });
  };

  private createMessage = (fileURL = null) => {
    const { currentUser } = this.props;
    const { uid, displayName, photoURL } = currentUser;
    const message: MessageVM = {
      createdAt: firebase.database.ServerValue.TIMESTAMP,
      user: {
        id: uid,
        name: displayName,
        photoURL: photoURL,
      },
    };
    if (fileURL !== null) {
      message.image = fileURL;
    } else {
      message.content = this.state.message;
    }
    return message;
  };

  private sendMessage = (): void => {
    const { message, typingRef } = this.state;
    const { currentChannel, currentUser, getMessageRef } = this.props;
    if (message) {
      this.setState({ loading: true });
      getMessageRef()
        .child(currentChannel.id)
        .push()
        .set(this.createMessage(null))
        .then(() => {
          this.setState({ loading: false, message: '', errors: [] });
          typingRef.child(currentChannel.id).child(currentUser.uid).remove();
        })
        .catch((err: any) => {
          console.error(err);
          this.setState({ loading: false, errors: { message: err.message } });
        });
    } else {
      this.setState({ errors: { message: 'Add a message' } });
    }
  };

  public getPath = () => {
    if (this.props.isPrivateChannel) {
      return `chat/private-${this.props.currentChannel.id}`;
    } else {
      return `chat/public`;
    }
  };

  private uploadFile = (file: any, metaDate: any) => {
    const pathToUpload = this.props.currentChannel.id;
    const { getMessageRef } = this.props;
    const filePath = `${this.getPath()}/${new Date().toISOString()}-${
      file.name
    }`;
    this.setState({ uploadState: 'uploading' });
    this.state.storageRef
      .child(filePath)
      .put(file, metaDate)
      .on(
        'state_changed',
        (snapshot) => {
          const progress = Math.round(
            (snapshot.bytesTransferred / snapshot.totalBytes) * 100
          );
          this.props.isProgressBarVisible(progress);
          this.setState({ percentUploaded: progress });
        },
        (err) => {
          console.error(err);
          this.setState({
            errors: {
              message: err.message,
            },
            uploadState: 'error',
            uploadTask: null,
          });
        },
        () => {
          storage
            .ref(filePath)
            .getDownloadURL()
            .then((downloadURL) => {
              this.sendFileMessage(downloadURL, getMessageRef(), pathToUpload);
            })
            .catch((err) => {
              console.error(err);
              this.setState({
                errors: {
                  message: err.message,
                },
                uploadState: 'error',
                uploadTask: null,
              });
            });
        }
      );
  };

  private sendFileMessage = (
    downloadURL: string | any,
    ref: any,
    pathToUpload: string | any
  ) => {
    ref
      .child(pathToUpload)
      .push()
      .set(this.createMessage(downloadURL))
      .then(() => {
        this.setState({
          uploadState: 'done',
        });
      })
      .catch((err: any) => {
        console.error(err);
        this.setState({
          errors: { message: err.message },
        });
      });
  };

  render() {
    const {
      errors,
      message,
      loading,
      modal,
      uploadState,
      percentUploaded,
      emojiPicker,
    } = this.state;
    return (
      <Segment className='message__form'>
        {emojiPicker && (
          <Picker
            set='google'
            onSelect={this.handleAddEmoji}
            title='Pick your emoji'
            emoji='point_up'
          />
        )}
        <Input
          fluid
          onChange={this.handleChange}
          onKeyDown={this.handleKeyDown}
          name='message'
          style={{ marginBottom: '0.7em' }}
          ref={(node) => (this.messageInputRef = node)}
          label={
            <Button
              icon={emojiPicker ? 'close' : 'add'}
              content={emojiPicker ? 'Close' : null}
              onClick={this.handleTogglePicker}
            />
          }
          labelPosition='left'
          className={errors.message ? 'error' : ''}
          placeholder='write your message'
          value={message}
        />

        <Button.Group icon widths='2'>
          <Button
            onClick={this.sendMessage}
            color='orange'
            content='Add Reply'
            labelPosition='left'
            icon='edit'
            disabled={message === '' || loading}
          />
          <Button
            color='teal'
            disabled={uploadState === 'uploading'}
            onClick={this.openModal}
            content='Upload Media'
            labelPosition='right'
            icon='cloud upload'
          />
        </Button.Group>
        <FileModal
          modal={modal}
          uploadFile={this.uploadFile}
          closeModal={this.closeModal}
        />
        <ProgressBar
          uploadState={uploadState}
          percentUploaded={percentUploaded}
        />
      </Segment>
    );
  }
}
export default MessageForm;
