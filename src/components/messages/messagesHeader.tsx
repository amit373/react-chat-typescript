import React, { PureComponent } from 'react';
import { Segment, Header, Icon, Input } from 'semantic-ui-react';

import { ChannelVM } from '../sidePanel/channels';
interface IProps {
  channelName: string;
  numUniqueUsers: string;
  handleSearchChange: any;
  searchLoading: boolean;
  isPrivateChannel: ChannelVM;
  isChannelStarred: boolean;
  handleStar: () => void;
}

class MessagesHeader extends PureComponent<IProps> {
  render() {
    const {
      channelName,
      numUniqueUsers,
      handleSearchChange,
      searchLoading,
      isPrivateChannel,
      isChannelStarred,
      handleStar,
    } = this.props;
    return (
      <Segment clearing>
        <Header fluid='true' as='h2' floated='left' style={{ marginBottom: 0 }}>
          <span>
            {channelName}
            {!isPrivateChannel && (
              <Icon
                onClick={handleStar}
                name={isChannelStarred ? 'star' : 'star outline'}
                color={isChannelStarred ? 'yellow' : 'black'}
              />
            )}
          </span>
          <Header.Subheader>{numUniqueUsers}</Header.Subheader>
        </Header>
        <Header floated='right'>
          <Input
            size='mini'
            loading={searchLoading}
            icon='search'
            name='searchTerm'
            onChange={handleSearchChange}
            placeholder='Search Messages'
          />
        </Header>
      </Segment>
    );
  }
}
export default MessagesHeader;
