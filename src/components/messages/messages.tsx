import React from 'react';
import { Segment, Comment } from 'semantic-ui-react';
import * as firebase from 'firebase';
import _ from 'lodash';

import MessagesHeader from './messagesHeader';
import MessageForm, { MessageVM } from './messageForm';
import Message from './message';
import Typing from './typing';

import { ChannelVM } from '../sidePanel/channels';
import { db } from '../../shared/firebase';
import { bindActionCreators, Dispatch } from 'redux';
import { connect } from 'react-redux';
import * as channelActions from './../../store/channel/channel.effect';
import Skeleton from './skeleton';

interface IProps {
  currentChannel: ChannelVM;
  isPrivateChannel: ChannelVM;
  currentUser: any;
  actions?: any;
}

class Messages extends React.PureComponent<IProps> {
  state = {
    privateChannel: this.props.isPrivateChannel,
    privateMessagesRef: db.ref('privateMessages'),
    messagesRef: db.ref('messages'),
    messages: [],
    messagesLoading: true,
    isChannelStarred: false,
    usersRef: db.ref('users'),
    numUniqueUsers: '',
    searchTerm: '',
    searchLoading: false,
    searchResults: [],
    typingRef: db.ref('typing'),
    typingUsers: [],
    connectedRef: db.ref('.info/connected'),
    listeners: [],
    progressBar: false,
  };

  messagesEnd: any = React.createRef();

  componentDidMount() {
    const { listeners } = this.state;
    const { currentChannel, currentUser } = this.props;
    if (currentChannel && currentUser) {
      this.removeListeners(listeners);
      this.addListeners(currentChannel.id);
      this.addUserStarsListener(currentChannel.id, currentUser.uid);
    }
  }

  componentWillUnmount() {
    this.removeListeners(this.state.listeners);
    this.state.connectedRef.off();
  }

  removeListeners = (listeners: Array<any>) => {
    listeners.forEach((listener: any) => {
      listener.ref.child(listener.id).off(listener.event);
    });
  };

  componentDidUpdate(prevProps: any, prevState: any) {
    if (this.messagesEnd) {
      this.scrollToBottom();
    }
  }

  addToListeners = (
    id: string | number,
    ref: firebase.database.Reference,
    event: any
  ) => {
    const index = this.state.listeners.findIndex((listener: any) => {
      return (
        listener.id === id && listener.ref === ref && listener.event === event
      );
    });

    if (index === -1) {
      const newListener: any = { id, ref, event };
      this.setState({ listeners: this.state.listeners.concat(newListener) });
    }
  };

  scrollToBottom = () => {
    this.messagesEnd.scrollIntoView({ behavior: 'smooth' });
  };

  addListeners = (channelId: string) => {
    this.addMessageListener(channelId);
    this.addTypingListeners(channelId);
  };

  addTypingListeners = (channelId: string) => {
    let typingUsers: Array<any> = [];
    this.state.typingRef.child(channelId).on('child_added', (snap) => {
      if (snap.key !== this.props.currentUser.uid) {
        typingUsers = typingUsers.concat({
          id: snap.key,
          name: snap.val(),
        });
        this.setState({ typingUsers });
      }
    });
    this.addToListeners(channelId, this.state.typingRef, 'child_added');

    this.state.typingRef.child(channelId).on('child_removed', (snap) => {
      const index = typingUsers.findIndex((user) => user.id === snap.key);
      if (index !== -1) {
        typingUsers = typingUsers.filter((user) => user.id !== snap.key);
        this.setState({ typingUsers });
      }
    });
    this.addToListeners(channelId, this.state.typingRef, 'child_removed');

    this.state.connectedRef.on('value', (snap) => {
      if (snap.val() === true) {
        this.state.typingRef
          .child(channelId)
          .child(this.props.currentUser.uid)
          .onDisconnect()
          .remove((err) => {
            if (err !== null) {
              console.error(err);
            }
          });
      }
    });
  };

  addMessageListener = (channelId: string) => {
    let loadedMessages: Array<any> = [];
    const ref = this.getMessagesRef();
    ref.child(channelId).on('child_added', (snap) => {
      loadedMessages.push(snap.val());
      this.setState({
        messages: loadedMessages,
        messagesLoading: false,
      });
      this.countUniqueUsers(loadedMessages);
      this.countUserPosts(loadedMessages);
    });
    this.addToListeners(channelId, ref, 'child_added');
  };

  addUserStarsListener = (channelId: string, userId: string) => {
    this.state.usersRef
      .child(userId)
      .child('starred')
      .once('value')
      .then((data) => {
        if (data.val() !== null) {
          const channelIds = Object.keys(data.val());
          const prevStarred = channelIds.includes(channelId);
          this.setState({ isChannelStarred: prevStarred });
        }
      });
  };

  getMessagesRef = () => {
    const { messagesRef, privateMessagesRef, privateChannel } = this.state;
    return privateChannel ? privateMessagesRef : messagesRef;
  };

  handleStar = () => {
    this.setState(
      (prevState: any) => ({
        isChannelStarred: !prevState.isChannelStarred,
      }),
      () => this.starChannel()
    );
  };

  starChannel = () => {
    const { currentChannel, currentUser } = this.props;
    if (this.state.isChannelStarred) {
      this.state.usersRef.child(`${currentUser.uid}/starred`).update({
        [currentChannel.id]: {
          name: currentChannel.name,
          details: currentChannel.details,
          createdBy: {
            name: currentChannel.createdBy.name,
            photoURL: currentChannel.createdBy.photoURL,
          },
        },
      });
    } else {
      this.state.usersRef
        .child(`${currentUser.uid}/starred`)
        .child(currentChannel.id)
        .remove((err) => {
          if (err !== null) {
            console.error(err);
          }
        });
    }
  };

  handleSearchChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    this.setState(
      {
        searchTerm: event.target.value,
        searchLoading: true,
      },
      () => this.handleSearchMessages()
    );
  };

  handleSearchMessages = () => {
    const channelMessages = [...this.state.messages];
    const regex = new RegExp(this.state.searchTerm, 'gi');
    const searchResults = channelMessages.reduce(
      (acc: any, message: MessageVM) => {
        if (
          (message.content && message.content.match(regex)) ||
          message.user.name.match(regex)
        ) {
          acc.push(message);
        }
        return acc;
      },
      []
    );
    this.setState({ searchResults });
    setTimeout(() => this.setState({ searchLoading: false }), 1000);
  };

  countUniqueUsers = (messages: Array<any>) => {
    const uniqueUsers = messages.reduce((acc, message) => {
      if (!acc.includes(message.user.name)) {
        acc.push(message.user.name);
      }
      return acc;
    }, []);
    const plural = uniqueUsers.length > 1 || uniqueUsers.length === 0;
    const numUniqueUsers = `${uniqueUsers.length} user${plural ? 's' : ''}`;
    this.setState({ numUniqueUsers });
  };

  countUserPosts = (messages: any) => {
    let userPosts = messages.reduce((acc: any, message: any) => {
      if (message.user.name in acc) {
        acc[message.user.name].count += 1;
      } else {
        acc[message.user.name] = {
          photoURL: message.user.photoURL,
          count: 1,
        };
      }
      return acc;
    }, {});
    this.props.actions.setUserPosts(userPosts);
  };

  displayMessages = (messages: any) =>
    messages.length > 0 &&
    messages.map((message: any) => (
      <Message
        key={message.timestamp}
        message={message}
        user={this.props.currentUser}
      />
    ));

  displayChannelName = (channel: ChannelVM) => {
    return channel
      ? `${this.state.privateChannel ? '@' : '#'}${channel.name}`
      : '';
  };

  displayTypingUsers = (users: any) =>
    users.length > 0 &&
    users.map((user: any) => (
      <div
        style={{ display: 'flex', alignItems: 'center', marginBottom: '0.2em' }}
        key={user.id}
      >
        <span className='user__typing'>{user.name} is typing</span> <Typing />
      </div>
    ));

  displayMessageSkeleton = (loading: boolean) =>
    loading ? (
      <React.Fragment>
        {[...Array(10)].map((_, i) => (
          <Skeleton key={i} />
        ))}
      </React.Fragment>
    ) : null;

  public isProgressBarVisible = (percent: number) => {
    if (percent > 0) {
      this.setState({ progressBar: true });
    }
  };

  render() {
    const {
      messages,
      progressBar,
      numUniqueUsers,
      searchTerm,
      searchResults,
      searchLoading,
      isChannelStarred,
      typingUsers,
      messagesLoading,
    } = this.state;
    const { currentChannel, currentUser, isPrivateChannel } = this.props;
    return (
      <>
        <MessagesHeader
          channelName={this.displayChannelName(currentChannel)}
          numUniqueUsers={numUniqueUsers}
          handleSearchChange={this.handleSearchChange}
          searchLoading={searchLoading}
          isPrivateChannel={isPrivateChannel}
          isChannelStarred={isChannelStarred}
          handleStar={this.handleStar}
        />
        <Segment>
          <Comment.Group
            className={progressBar ? 'messages__progress' : 'messages'}
          >
            {this.displayMessageSkeleton(messagesLoading)}
            {searchTerm
              ? this.displayMessages(searchResults)
              : this.displayMessages(messages)}
            {this.displayTypingUsers(typingUsers)}
            <div ref={(node) => (this.messagesEnd = node)}></div>
          </Comment.Group>
        </Segment>
        <MessageForm
          currentChannel={currentChannel}
          currentUser={currentUser}
          isProgressBarVisible={this.isProgressBarVisible}
          isPrivateChannel={isPrivateChannel}
          getMessageRef={this.getMessagesRef}
        />
      </>
    );
  }
}

const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    actions: {
      ...bindActionCreators(channelActions, dispatch),
    },
  };
};
export default connect(null, mapDispatchToProps)(Messages);
