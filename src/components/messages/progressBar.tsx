import React from 'react';
import { Progress } from 'semantic-ui-react';

interface IProps {
  uploadState: string;
  percentUploaded: number;
}

const ProgressBar: React.FC<IProps> = ({ uploadState, percentUploaded }) => (
  <>
    {uploadState === 'uploading' && (
      <Progress
        className='progress__bar'
        percent={percentUploaded}
        progress='percent'
        indicating
        size='medium'
        inverted
      />
    )}
  </>
);

export default ProgressBar;
