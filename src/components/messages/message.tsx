import React from 'react';
import moment from 'moment';

import { Comment, Image } from 'semantic-ui-react';
import { MessageVM } from './messageForm';

interface IProps {
  message: MessageVM;
  user: any;
}

const isOwnMessage = (message: MessageVM, user: any) => {
  return message.user.id === user.uid ? 'message_self' : '';
};

const isImage = (message: MessageVM) => {
  return message.hasOwnProperty('image') && !message.hasOwnProperty('content');
};

const timeFromNow = (timeStamp: Date | Object) => moment(timeStamp).fromNow();

const Message: React.FC<IProps> = ({ message, user }) => {
  return (
    <Comment>
      <Comment.Avatar src={message.user.photoURL} />
      <Comment.Content className={isOwnMessage(message, user)}>
        <Comment.Author as='a'>{message.user.name}</Comment.Author>
        <Comment.Metadata>{timeFromNow(message.createdAt)}</Comment.Metadata>
        {isImage(message) ? (
          <Image src={message.image} className='message__image' />
        ) : (
          <Comment.Text>{message.content}</Comment.Text>
        )}
      </Comment.Content>
    </Comment>
  );
};
export default Message;
