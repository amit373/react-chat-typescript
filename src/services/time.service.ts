import moment from 'moment';

export const getRangeOfDates = (startAt: string, endAt: string): string[] => {
    const dates: Array<any> = [];
    let mStartAt = moment(new Date(startAt));
    const mEndAt = moment(new Date(endAt));

    while (mStartAt < mEndAt) {
        dates.push(mStartAt.format());
        mStartAt = mStartAt.add(1, 'day');
    }
    return dates;
}

export const isDateInPast = (date: moment.Moment): boolean => {
    return date.diff(moment(), 'days') < 0;
}

export const format = (date: string | Object | Date, dateFormat = 'YYYY/MM/DD'): string => {
    if (!date) { return ''; }
    return moment(date).format(dateFormat);
}

export const calendar = (date: string): any => {
    if (!date) { return ''; }
    return moment(date).calendar(date, {
        sameDay: '[Today]',
        nextDay: '[Tomorrow]',
        lastDay: '[Yesterday]',
        nextWeek: 'dddd MMM DD',
        lastWeek: '[Last] dddd',
        sameElse: 'DD/MM/YYYY'
    })
}

export const fromNow = (date: string): any => {
    if (!date) { return ''; }
    return moment(date).fromNow();
}