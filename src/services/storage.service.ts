export const saveToLocalStorage = (key: any, value: any): void => {
    localStorage[key] = JSON.stringify(value);
}

export const getFromLocalStorage = (key: any): any => {
    if (localStorage[key]) {
        return JSON.parse(localStorage[key]);
    } else {
        return false;
    }
}

export const removeFromLocalStorage = (key: any): any => {
    localStorage.removeItem(key);
}

export const saveToSession = (key: any, value: any): void => {
    sessionStorage[key] = JSON.stringify(value);
}

export const getSessionStorage = (key: any): any => {
    if (sessionStorage[key]) {
        return JSON.parse(sessionStorage[key]);
    } else {
        return false;
    }
}

export const removeFromSessionStorage = (key: any): any => {
    sessionStorage.removeItem(key);
}

export const resetStorage = (): void => {
    localStorage.clear();
    sessionStorage.clear();
}
